import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:startup/SplashScreen.dart';
import 'package:startup/CustomClasses/errorDialog.dart';
import 'package:startup/CustomClasses/loadingDialog.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String title = "Startup";

  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: title,
      theme: ThemeData(primarySwatch: Colors.teal, fontFamily: "vazir"),
      home: FutureBuilder(
        future: _initialization,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return ErrorAlertDialog(message: "Connection Error!");
          } else if (snapshot.connectionState == ConnectionState.done) {
            return SplashScreen();
          } else
            return LoadingAlertDialog(
              message: "Connecting...",
              onBackButton: true,
            );
        },
      ),
    );
  }
}
