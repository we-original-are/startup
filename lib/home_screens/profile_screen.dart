import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:photo_view/photo_view.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:startup/DataManager.dart';

class ProfileScreen extends StatefulWidget {
  final String profileURL;

  const ProfileScreen({Key? key, required this.profileURL}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  String imageUrl = "";
  String userEmail = "";
  bool showAppbar = false;
  bool profileProgress = false;

  @override
  void initState() {
    setState(() {
      imageUrl = widget.profileURL;
      userEmail = _auth.currentUser!.email!;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width,
        height = MediaQuery.of(context).size.height;
    return showAppbar
        ? Scaffold(
            appBar: AppBar(
              title: Text("Profile"),
              automaticallyImplyLeading: false,
              leading: BackButton(
                onPressed: () {
                  Navigator.of(context).pop("ok");
                },
              ),
              actions: [
                IconButton(
                    onPressed: () {
                      displayBottomSheet(context);
                    },
                    icon: Icon(Icons.more_vert)),
              ],
            ),
            body: Container(
              width: width,
              height: height,
              child: GestureDetector(
                  onTap: () {
                    if (showAppbar) {
                      setState(() {
                        showAppbar = false;
                      });
                    } else {
                      setState(() {
                        showAppbar = true;
                      });
                    }
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Center(
                          child: imageUrl.isNotEmpty
                              ? PhotoView(
                                  imageProvider: NetworkImage(imageUrl),
                                )
                              : Icon(
                                  Icons.no_photography,
                                  size: 300,
                                  color: Colors.grey,
                                )),
                      profileProgress ? RefreshProgressIndicator() : Text("")
                    ],
                  )),
            ))
        : Scaffold(
            body: Container(
            width: width,
            height: height,
            child: GestureDetector(
                onTap: () {
                  if (showAppbar) {
                    setState(() {
                      showAppbar = false;
                    });
                  } else {
                    setState(() {
                      showAppbar = true;
                    });
                  }
                },
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Center(
                        child: imageUrl.isNotEmpty
                            ? PhotoView(
                                imageProvider: NetworkImage(imageUrl),
                              )
                            : Icon(
                                Icons.no_photography,
                                size: 300,
                                color: Colors.grey,
                              )),
                    profileProgress ? RefreshProgressIndicator() : Text("")
                  ],
                )),
          ));
  }

  Future<void> _getImage(ImageSource imageSource) async {
    PickedFile? imageFile = await ImagePicker.platform
        .pickImage(source: imageSource, imageQuality: 100);

    if (imageFile != null) {
      if (imageUrl.isNotEmpty) {
        updateProfile(imageFile.path);
      } else {
        uploadProfile(imageFile.path);
      }
    }
  }

  Future<void> deleteProfile() async {
    try {
      await firebase_storage.FirebaseStorage.instance
          .ref('profile/$userEmail-image-file.png')
          .delete();
      setState(() {
        imageUrl = "";
        DataManager.profileUrl = "";
        DataManager.profileChanged = true;
        profileProgress = false;
      });
    } on firebase_storage.FirebaseException catch (e) {
      print(e.code);
      setState(() {
        profileProgress = false;
      });
    }
  }

  Future<void> updateProfile(String filePath) async {
    try {
      await firebase_storage.FirebaseStorage.instance
          .ref('profile/$userEmail-image-file.png')
          .delete();
      setState(() {
        profileProgress = true;
      });
      await uploadProfile(filePath);
    } on firebase_storage.FirebaseException catch (e) {
      print(e.code);
      setState(() {
        profileProgress = false;
      });
    }
  }

  Future<void> uploadProfile(String filePath) async {
    if (filePath.isNotEmpty) {
      File file = File(filePath);
      try {
        await firebase_storage.FirebaseStorage.instance
            .ref('profile/$userEmail-image-file.png')
            .putFile(file);
        DataManager.profileChanged = true;
        await fetchProfileUrl();
      } on firebase_storage.FirebaseException catch (e) {
        print(e.code);
        setState(() {
          profileProgress = false;
        });
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text(
              "Upload Error! ",
              style: TextStyle(color: Colors.white),
            ),
          ),
        );
      }
    }
  }

  Future<void> fetchProfileUrl() async {
    String email = _auth.currentUser!.email.toString();
    try {
      String photoUrl = await firebase_storage.FirebaseStorage.instance
          .ref('profile/$email-image-file.png')
          .getDownloadURL();
      if (photoUrl.isNotEmpty) {
        setState(() {
          imageUrl = photoUrl;
          DataManager.profileUrl = photoUrl;
          DataManager.profileChanged = true;
          profileProgress = false;
        });
      }
    } on firebase_storage.FirebaseException catch (e) {
      print(e.code);
    }
  }

  displayBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10))),
        builder: (context) {
          return Container(
            height: MediaQuery.of(context).size.height * 0.3,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text(
                    "Profile Image",
                    style: TextStyle(fontSize: 18),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        FloatingActionButton(
                          onPressed: () {
                            _getImage(ImageSource.camera);
                            Navigator.pop(context);
                          },
                          child: Icon(Icons.linked_camera),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text("Camera"),
                        )
                      ],
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        FloatingActionButton(
                          onPressed: () {
                            _getImage(ImageSource.gallery);
                            Navigator.pop(context);
                          },
                          child: Icon(Icons.image),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            "Gallery",
                          ),
                        )
                      ],
                    ),
                    imageUrl.isNotEmpty
                        ? Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              FloatingActionButton(
                                backgroundColor: Colors.red,
                                onPressed: () {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          content: Text(
                                              "Do you want delete profile image?"),
                                          actions: [
                                            TextButton(
                                                onPressed: () {
                                                  setState(() {
                                                    profileProgress = true;
                                                  });
                                                  deleteProfile();
                                                  Navigator.pop(context);
                                                  Navigator.pop(context);
                                                },
                                                child: Text("Yes")),
                                            TextButton(
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                  Navigator.pop(context);
                                                },
                                                child: Text("Cancel")),
                                          ],
                                        );
                                      });
                                },
                                child: Icon(
                                  Icons.delete,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("Delete Image"),
                              )
                            ],
                          )
                        : Text(""),
                  ],
                ),
              ],
            ),
          );
        });
  }
}
