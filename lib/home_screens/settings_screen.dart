import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:startup/CustomClasses/errorDialog.dart';
import 'package:startup/CustomClasses/loadingDialog.dart';
import 'package:startup/DataManager.dart';
import 'package:startup/SplashScreen.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:startup/home_screens/edit_account.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Settings"),
      ),
      body: Column(
        children: [
          ListTile(
            leading: Icon(
              Icons.account_circle,
              color: Theme.of(context).primaryColor,
            ),
            title: Text("Edit account"),
            onTap: () {
              Route route = MaterialPageRoute(builder: (context) {
                return EditAccount();
              });
              Navigator.push(context, route);
            },
          ),
          ListTile(
            leading: Icon(
              Icons.exit_to_app,
              color: Theme.of(context).primaryColor,
            ),
            title: Text("Log out"),
            onTap: () {
              accountControlDialog(true);
            },
          ),
          Divider(
            color: Colors.grey,
            thickness: 1,
            endIndent: 5.0,
            indent: 5.0,
          ),
          ListTile(
            leading: Icon(
              Icons.no_accounts,
              color: Colors.red,
            ),
            title: Text(
              "Delete account",
              style: TextStyle(color: Colors.red),
            ),
            onTap: () {
              DataManager.userEmail = _auth.currentUser!.email!;
              DataManager.userId = _auth.currentUser!.uid;
              accountControlDialog(false);
            },
          ),
        ],
      ),
    );
  }

  accountControlDialog(bool logOut) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            titlePadding: EdgeInsets.zero,
            title: Container(
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10),
                        topLeft: Radius.circular(10))),
                padding: EdgeInsets.all(10),
                child: Text(
                  logOut ? "Log out" : "Delete account",
                  style: TextStyle(
                      color: Theme.of(context).primaryIconTheme.color),
                )),
            content: logOut
                ? Text("Do you want exit from app?")
                : Text(
                    "This will permanently delete all information! However, do you want to continue?"),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    logOut ? signOut() : deleteUserData();
                  },
                  child: Text('yes')),
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('cancel')),
            ],
          );
        });
  }

  Future deleteUserData() async {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return LoadingAlertDialog(
            message: "Deleting Account ...",
            onBackButton: false,
          );
        });
    String userId = _auth.currentUser!.uid;
    try {
      await FirebaseFirestore.instance.collection('users').doc(userId).delete();
      await deleteUserProfile();
      await deleteUserAuth();
    } catch (e) {
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'Connection Error!',
            style: TextStyle(color: Colors.white),
          ),
        ),
      );
    }
  }

  Future deleteUserProfile() async {
    String userEmail = _auth.currentUser!.email.toString();
    try {
      await firebase_storage.FirebaseStorage.instance
          .ref('profile/$userEmail-image-file.png')
          .delete();
    } on firebase_storage.FirebaseException catch (e) {
      print(e.code);
    }
  }

  Future deleteUserAuth() async {
    try {
      await FirebaseAuth.instance.currentUser!.delete();
      await signOut();
    } on FirebaseAuthException catch (e) {
      print(e.code);
      Navigator.pop(context);
      await signOut();
    }
  }

  Future signOut() async {
    await FirebaseAuth.instance.signOut().then((_) {
      Route newRoute = MaterialPageRoute(builder: (context) {
        return SplashScreen();
      });
      Navigator.pushAndRemoveUntil(context, newRoute, (route) => false);
    });
  }

  showErrorScreen(String message) {
    Route route = MaterialPageRoute(builder: (BuildContext context) {
      return ErrorAlertDialog(message: message);
    });
    Navigator.push(context, route);
  }
}
