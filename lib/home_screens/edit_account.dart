import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:startup/CustomClasses/customTextField.dart';
import 'package:startup/DataManager.dart';

class EditAccount extends StatefulWidget {
  const EditAccount({Key? key}) : super(key: key);

  @override
  _EditAccountState createState() => _EditAccountState();
}

class _EditAccountState extends State<EditAccount> {
  TextEditingController _controller = TextEditingController();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  bool _showLoading = false;

  @override
  void initState() {
    _controller.text = DataManager.userName;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width,
        height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit account"),
      ),
      body: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("images/pic.jpg"), fit: BoxFit.cover)),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Edit User Name ",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 10,
              ),
              CustomTextField(
                  controller: _controller,
                  icon: Icons.account_circle,
                  labelText: "user name",
                  isObscure: false,
                  textInputType: TextInputType.text),
              _showLoading
                  ? Center(child: RefreshProgressIndicator())
                  : Container(
                      margin: EdgeInsets.all(10),
                      width: kIsWeb ? width / 2 : width / 1.5,
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10))),
                          onPressed: () {
                            if (_controller.text.trim().characters !=
                                    DataManager.userName.characters &&
                                _controller.text.trim().isNotEmpty) {
                              setState(() {
                                _showLoading = true;
                              });
                              updateUserData(_controller.text.trim());
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              "Submit",
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color:
                                      Theme.of(context).primaryIconTheme.color),
                            ),
                          )),
                    )
            ],
          ),
        ),
      ),
    );
  }

  Future updateUserData(String userName) async {
    Map<String, dynamic> userData = {
      'userName': userName,
    };
    String userId = _auth.currentUser!.uid;

    try {
      await FirebaseFirestore.instance
          .collection('users')
          .doc(userId)
          .update(userData);
      DataManager.userNameChanged = true;
      DataManager.userName = userName;
      setState(() {
        _showLoading = false;
      });
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'Updated Successfully',
            style: TextStyle(color: Colors.white),
          ),
        ),
      );
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            "Update Error! ",
            style: TextStyle(color: Colors.white),
          ),
        ),
      );
    }
  }
}
