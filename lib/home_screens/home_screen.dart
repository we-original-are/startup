import 'dart:async';
import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:startup/AvatarColor.dart';
import 'package:startup/DataManager.dart';
import 'package:startup/home_screens/settings_screen.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:startup/home_screens/profile_screen.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  String profileURL = "";
  String userEmail = "";
  String userName = "";
  Color avatarColor = Colors.white;
  String avatarName = "";
  bool loading = false;
  bool profileProgress = false;

  void getColor() async {
    Color color = await ColorManager().getColorIndex();
    setState(() {
      avatarColor = color;
    });
  }

  @override
  void initState() {
    getColor();
    fetchProfileUrl();
    fetchUserData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Startup"),
      ),
      drawer: Drawer(
        child: Column(
          children: [
            Stack(
              alignment: Alignment.center,
              children: [
                UserAccountsDrawerHeader(
                  currentAccountPicture: circleAvatarCustom(),
                  accountName: Text(userName),
                  accountEmail: Text(userEmail),
                ),
                FloatingActionButton(
                    elevation: 5,
                    mini: true,
                    backgroundColor: Colors.white,
                    onPressed: () {
                      displayBottomSheet(context);
                    },
                    child: Icon(
                      Icons.photo_camera_rounded,
                      color: Theme.of(context).primaryColor,
                    ))
              ],
            ),
            ListTile(
              leading: Icon(
                Icons.settings,
                color: Theme.of(context).primaryColor,
              ),
              title: Text("settings"),
              onTap: () {
                Route route = MaterialPageRoute(builder: (context) {
                  return SettingsPage();
                });
                Navigator.push(context, route).then((value) {
                  if (DataManager.userNameChanged) {
                    String name = DataManager.userName;
                    var namePlus = name.split(" ");
                    setState(() {
                      avatarName = namePlus.length > 1
                          ? (namePlus[0].substring(0, 1) +
                              " " +
                              namePlus[1].substring(0, 1))
                          : name.substring(0, 1);
                      userName = DataManager.userName;
                      userEmail = _auth.currentUser!.email!;
                      loading = false;
                    });
                    DataManager.userNameChanged = false;
                  }
                });
              },
            ),
          ],
        ),
      ),
      body: Center(
        child: loading
            ? RefreshProgressIndicator()
            : Text(
                "Hello World!",
                style: Theme.of(context).textTheme.headline5,
              ),
      ),
    );
  }

  Future<void> fetchProfileUrl() async {
    setState(() {
      profileProgress = true;
    });
    String email = _auth.currentUser!.email.toString();
    try {
      await firebase_storage.FirebaseStorage.instance
          .ref('profile/$email-image-file.png')
          .getDownloadURL()
          .then((value) {
        if (value.isNotEmpty) {
          setState(() {
            profileURL = value;
            profileProgress = false;
          });
        }
      });
    } on firebase_storage.FirebaseException catch (e) {
      setState(() {
        profileProgress = false;
      });
    }
  }

  Future<void> fetchUserData() async {
    setState(() {
      loading = true;
    });
    Map<String, dynamic> userData = {};
    String userId = _auth.currentUser!.uid;

    try {
      await FirebaseFirestore.instance
          .collection('users')
          .doc(userId)
          .get()
          .then((value) {
        userData = value.data()!;
      });
      var name = userData['userName'];
      DataManager.userName = name;
      var namePlus = name.split(" ");
      setState(() {
        avatarName = namePlus.length > 1
            ? (namePlus[0].substring(0, 1) + " " + namePlus[1].substring(0, 1))
            : name.substring(0, 1);
        userName = userData['userName'];
        userEmail = _auth.currentUser!.email!;
        loading = false;
      });
    } on firebase_storage.FirebaseException catch (e) {
      setState(() {
        loading = false;
      });
    }
  }

  circleAvatarCustom() {
    return CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 55,
        child: Stack(
          alignment: Alignment.center,
          children: [
            profileURL.isEmpty
                ? CircleAvatar(
                    radius: 50,
                    backgroundColor: avatarColor,
                    child: Text(
                      avatarName,
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                    ),
                  )
                : CircleAvatar(
                    radius: 50,
                    backgroundImage: NetworkImage(profileURL),
                  ),
            profileProgress
                ? RefreshProgressIndicator()
                : MaterialButton(
                    elevation: 5,
                    onPressed: () {
                      profileURL.isEmpty
                          ? displayBottomSheet(context)
                          : showProfileScreen();
                    },
                    padding: EdgeInsets.zero,
                    splashColor: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(100))),
                    child: CircleAvatar(
                      radius: 50,
                      backgroundColor: Colors.transparent,
                    ),
                  ),
          ],
        ));
  }

  Future _getImage(ImageSource imageSource) async {
    PickedFile? imageFile = await ImagePicker.platform
        .pickImage(source: imageSource, imageQuality: 100);

    if (imageFile != null) {
      if (profileURL.isNotEmpty) {
        setState(() {
          profileProgress = true;
        });
        updateProfile(imageFile.path);
      } else {
        setState(() {
          profileProgress = true;
        });
        uploadProfile(imageFile.path);
      }
    }
  }

  showProfileScreen() {
    Route route = MaterialPageRoute(builder: (BuildContext context) {
      return ProfileScreen(profileURL: profileURL);
    });
    Navigator.push(context, route).then((value) {
      if (DataManager.profileChanged) {
        setState(() {
          profileURL = DataManager.profileUrl;
          DataManager.profileChanged = false;
        });
      }
    });
  }

  Future<void> deleteProfile() async {
    try {
      await firebase_storage.FirebaseStorage.instance
          .ref('profile/$userEmail-image-file.png')
          .delete();
      setState(() {
        profileURL = "";
        profileProgress = false;
      });
    } on firebase_storage.FirebaseException catch (e) {
      print(e.code);
      setState(() {
        profileProgress = false;
      });
    }
  }

  Future<void> updateProfile(String filePath) async {
    try {
      await firebase_storage.FirebaseStorage.instance
          .ref('profile/$userEmail-image-file.png')
          .delete();
      await uploadProfile(filePath);
    } on firebase_storage.FirebaseException catch (e) {
      print(e.code);

      setState(() {
        profileProgress = false;
      });
    }
  }

  Future<void> uploadProfile(String filePath) async {
    if (filePath.isNotEmpty) {
      File file = File(filePath);
      try {
        await firebase_storage.FirebaseStorage.instance
            .ref('profile/$userEmail-image-file.png')
            .putFile(file);
        await fetchProfileUrl();
      } on firebase_storage.FirebaseException catch (e) {
        print(e.code);
        setState(() {
          profileProgress = false;
        });
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text(
              "Upload Error! ",
              style: TextStyle(color: Colors.white),
            ),
          ),
        );
      }
    }
  }

  displayBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10))),
        builder: (context) {
          return Container(
            height: MediaQuery.of(context).size.height * 0.3,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text(
                    "Profile Image",
                    style: TextStyle(fontSize: 18),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        FloatingActionButton(
                          onPressed: () {
                            _getImage(ImageSource.camera);
                            Navigator.pop(context);
                          },
                          child: Icon(Icons.linked_camera),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text("Camera"),
                        )
                      ],
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        FloatingActionButton(
                          onPressed: () {
                            _getImage(ImageSource.gallery);
                            Navigator.pop(context);
                          },
                          child: Icon(Icons.image),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            "Gallery",
                          ),
                        )
                      ],
                    ),
                    profileURL.isNotEmpty
                        ? Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              FloatingActionButton(
                                backgroundColor: Colors.red,
                                onPressed: () {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          content: Text(
                                              "Do you want delete profile image?"),
                                          actions: [
                                            TextButton(
                                                onPressed: () {
                                                  setState(() {
                                                    profileProgress = true;
                                                  });
                                                  deleteProfile();
                                                  Navigator.pop(context);
                                                  Navigator.pop(context);
                                                },
                                                child: Text("Yes")),
                                            TextButton(
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                  Navigator.pop(context);
                                                },
                                                child: Text("Cancel")),
                                          ],
                                        );
                                      });
                                },
                                child: Icon(
                                  Icons.delete,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("Delete Image"),
                              )
                            ],
                          )
                        : Text(""),
                  ],
                ),
              ],
            ),
          );
        });
  }
}
