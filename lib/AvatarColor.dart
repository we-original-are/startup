import 'dart:math';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ColorManager {
  List<Color> _colors = <Color>[
    Colors.blue,
    Colors.blueAccent,
    Colors.red,
    Colors.redAccent,
    Colors.green,
    Colors.greenAccent,
    Colors.yellow,
    Colors.yellowAccent,
    Colors.pink,
    Colors.pinkAccent,
    Colors.lightBlue,
    Colors.lightGreen,
    Colors.indigo,
    Colors.purple,
    Colors.deepPurple,
    Colors.deepPurpleAccent,
    Colors.orange,
    Colors.orangeAccent,
  ];
  Random random = new Random();

  Future<void> setColorIndex() async {
    int index = random.nextInt(_colors.length - 1);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('index', index);
  }

  Future<Color> getColorIndex() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int index = prefs.getInt('index')!.toInt();
    return _colors[index];
  }
}
