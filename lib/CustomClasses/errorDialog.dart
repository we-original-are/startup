import 'package:flutter/material.dart';

class ErrorAlertDialog extends StatelessWidget {
  final String message;

  ErrorAlertDialog({required this.message});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width,
        height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("images/pic.jpg"), fit: BoxFit.cover)),
        child: AlertDialog(
          titlePadding: EdgeInsets.zero,
          contentPadding: EdgeInsets.all(10.0),
          title: Container(
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10),
                    topLeft: Radius.circular(10))),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(
                "Error Message",
                style:
                    TextStyle(color: Theme.of(context).primaryIconTheme.color),
              ),
            ),
          ),
          content: SingleChildScrollView(child: Text(message)),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10))),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("OK"))
          ],
        ),
      ),
    );
  }
}
