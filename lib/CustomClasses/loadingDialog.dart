import 'package:flutter/material.dart';
import 'package:startup/CustomClasses/loadingWidget.dart';

class LoadingAlertDialog extends StatelessWidget {
  final String message;
  final bool onBackButton;

  LoadingAlertDialog({required this.message, required this.onBackButton});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => onBackButton,
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Theme.of(context).primaryColor, Colors.tealAccent],
                  begin: Alignment.center,
                  end: Alignment.bottomRight)),
          child: AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            content: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                circularProgress(context),
                SizedBox(
                  height: 10,
                ),
                Text(
                  message,
                  style: TextStyle(color: Theme.of(context).primaryColor),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
