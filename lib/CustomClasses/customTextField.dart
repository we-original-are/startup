import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  final TextEditingController controller;
  final IconData icon;
  final String labelText;
  final bool isObscure;
  final TextInputType textInputType;

  const CustomTextField(
      {Key? key,
      required this.controller,
      required this.icon,
      required this.labelText,
      required this.isObscure,
      required this.textInputType})
      : super(key: key);

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool showPassword = true;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
        padding: EdgeInsets.all(8.0),
        margin: EdgeInsets.all(10.0),
        width: kIsWeb ? width / 2 : width / 1.5,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: widget.isObscure
            ? TextFormField(
                keyboardType: widget.textInputType,
                obscureText: showPassword,
                controller: widget.controller,
                cursorColor: Theme.of(context).primaryColor,
                decoration: InputDecoration(
                    suffixIcon: IconButton(
                        onPressed: () {
                          if (showPassword) {
                            setState(() {
                              showPassword = false;
                            });
                          } else {
                            setState(() {
                              showPassword = true;
                            });
                          }
                        },
                        icon: showPassword
                            ? Icon(Icons.visibility_off_rounded)
                            : Icon(Icons.visibility_rounded)),
                    border: InputBorder.none,
                    prefixIcon: Icon(
                      widget.icon,
                      color: Theme.of(context).primaryColor,
                    ),
                    focusColor: Theme.of(context).primaryColor,
                    labelText: widget.labelText))
            : TextFormField(
                keyboardType: widget.textInputType,
                obscureText: widget.isObscure,
                controller: widget.controller,
                cursorColor: Theme.of(context).primaryColor,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    prefixIcon: Icon(
                      widget.icon,
                      color: Theme.of(context).primaryColor,
                    ),
                    focusColor: Theme.of(context).primaryColor,
                    labelText: widget.labelText),
              ));
  }
}

class NewTextFormField extends StatelessWidget {
  final IconData icon;
  final String labelText;
  final bool isObscure;
  final TextInputType textInputType;
  final TextEditingController controller;

  const NewTextFormField(
      {Key? key,
      required this.icon,
      required this.labelText,
      required this.isObscure,
      required this.textInputType,
      required this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _screenWidth = MediaQuery.of(context).size.width;
    return Container(
      padding: EdgeInsets.all(8.0),
      margin: EdgeInsets.all(10.0),
      width: _screenWidth / 2,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: TextFormField(
        controller: controller,
        keyboardType: textInputType,
        obscureText: isObscure,
        cursorColor: Theme.of(context).primaryColor,
        decoration: InputDecoration(
            border: InputBorder.none,
            prefixIcon: Icon(
              icon,
              color: Colors.cyan,
            ),
            focusColor: Theme.of(context).primaryColor,
            labelText: labelText),
      ),
    );
  }
}
