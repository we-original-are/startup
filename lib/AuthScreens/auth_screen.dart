import 'package:flutter/material.dart';
import 'package:startup/AuthScreens/Login.dart';
import 'package:startup/AuthScreens/Register.dart';

class AuthScreen extends StatefulWidget {
  const AuthScreen({Key? key}) : super(key: key);

  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  String title = "Startup";

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        width: width,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("images/pic.jpg"), fit: BoxFit.cover)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              title,
              style: TextStyle(
                  fontSize: 50,
                  color: Theme.of(context).primaryIconTheme.color,
                  fontFamily: "dastnevis",
                  fontWeight: FontWeight.bold,
                  letterSpacing: 2.0),
            ),
            SizedBox(
              height: 30,
            ),
            SizedBox(
                width: width / 2,
                child: InkWell(
                  onTap: () {
                    Route route = MaterialPageRoute(builder: (context) {
                      return Login();
                    });
                    Navigator.push(context, route);
                  },
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Theme.of(context).primaryIconTheme.color,
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    ),
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      "Login",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).primaryColor),
                    ),
                  ),
                )),
            SizedBox(
              height: 20,
            ),
            InkWell(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              onTap: () {
                Route route = MaterialPageRoute(builder: (context) {
                  return Register();
                });
                Navigator.push(context, route);
              },
              child: Container(
                width: width / 2,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: Colors.white10,
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    border: Border.all(
                        color: Theme.of(context).primaryIconTheme.color!,
                        width: 1.5)),
                child: Text(
                  "Register",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Theme.of(context).primaryIconTheme.color!,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
