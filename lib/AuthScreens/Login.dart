import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:startup/AvatarColor.dart';
import 'package:startup/CustomClasses/errorDialog.dart';
import 'package:startup/CustomClasses/loadingDialog.dart';
import 'package:startup/CustomClasses/customTextField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:startup/home_screens/home_screen.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width,
        height = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Login",
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      body: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("images/pic.jpg"), fit: BoxFit.cover)),
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                CustomTextField(
                    controller: _emailController,
                    icon: Icons.email,
                    labelText: "email",
                    isObscure: false,
                    textInputType: TextInputType.emailAddress),
                CustomTextField(
                    controller: _passwordController,
                    icon: Icons.lock,
                    labelText: "password",
                    isObscure: true,
                    textInputType: TextInputType.visiblePassword),
                Container(
                  margin: EdgeInsets.all(10),
                  width: kIsWeb ? width / 2 : width / 1.5,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10))),
                      onPressed: () {
                        if (_emailController.text.trim().isNotEmpty &&
                            _passwordController.text.trim().isNotEmpty) {
                          if (!RegExp(
                                  "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
                              .hasMatch(_emailController.text.trim())) {
                            showErrorScreen("Invalid email entered");
                          } else if (_passwordController.text.trim().length <
                              6) {
                            showErrorScreen(
                                "Password must be 6 characters or more");
                          } else {
                            login();
                          }
                        } else {
                          showErrorScreen(
                              "All fields must be completed to login");
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          "Login",
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color:
                                  Theme.of(context).primaryIconTheme.color),
                        ),
                      )),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  login() async {
    showDialog(
        context: context,
        builder: (context) {
          return LoadingAlertDialog(
            message: "Please Wait...",
            onBackButton: true,
          );
        });

    try {
      await _auth
          .signInWithEmailAndPassword(
              email: _emailController.text.trim(),
              password: _passwordController.text.trim())
          .then((value) async {
        await setColorIndex();
        Navigator.pop(context);
        Route newRoute = MaterialPageRoute(builder: (context) {
          return HomePage();
        });
        Navigator.pushAndRemoveUntil(context, newRoute, (route) => false);
      });
    } on FirebaseAuthException catch (e) {
      Navigator.pop(context);
      showErrorScreen(e.code);
    }
  }

  showErrorScreen(String message) {
    Route route = MaterialPageRoute(builder: (BuildContext context) {
      return ErrorAlertDialog(message: message);
    });
    Navigator.push(context, route);
  }

  Future<void> setColorIndex() async {
    await ColorManager().setColorIndex();
  }
}
