import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:startup/AvatarColor.dart';
import 'package:startup/CustomClasses/errorDialog.dart';
import 'package:startup/CustomClasses/loadingDialog.dart';
import 'package:startup/CustomClasses/customTextField.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:startup/home_screens/home_screen.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passwordConfirmController =
      TextEditingController();

  String profilePatch = "";

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width,
        height = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Register",
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      body: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("images/pic.jpg"), fit: BoxFit.cover)),
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              CircleAvatar(
                  backgroundColor: Colors.transparent,
                  radius: 55,
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      profilePatch.isNotEmpty
                          ? CircleAvatar(
                              radius: 50,
                              backgroundColor: Theme.of(context).primaryColor,
                              backgroundImage: FileImage(File(profilePatch)),
                            )
                          : CircleAvatar(
                              radius: 50,
                              backgroundColor: Theme.of(context).primaryColor,
                              child: Icon(
                                Icons.camera_alt,
                                color: Theme.of(context).primaryIconTheme.color,
                                size: 40,
                              ),
                            ),
                      MaterialButton(
                        elevation: 5,
                        onPressed: () {
                          displayBottomSheet(context);
                        },
                        padding: EdgeInsets.zero,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(100))),
                        child: CircleAvatar(
                          radius: 50,
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                    ],
                  )),
              SizedBox(
                height: 10,
              ),
              Column(
                children: [
                  CustomTextField(
                      controller: _userNameController,
                      icon: Icons.account_circle_rounded,
                      labelText: "user Name",
                      isObscure: false,
                      textInputType: TextInputType.name),
                  CustomTextField(
                      controller: _emailController,
                      icon: Icons.email,
                      labelText: "email",
                      isObscure: false,
                      textInputType: TextInputType.emailAddress),
                  CustomTextField(
                      controller: _passwordController,
                      icon: Icons.lock,
                      labelText: "password",
                      isObscure: true,
                      textInputType: TextInputType.visiblePassword),
                  CustomTextField(
                      controller: _passwordConfirmController,
                      icon: Icons.lock,
                      labelText: "confirm Password",
                      isObscure: true,
                      textInputType: TextInputType.visiblePassword),
                  Container(
                    margin: EdgeInsets.all(10),
                    width: kIsWeb ? width / 2 : width / 1.5,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10))),
                        onPressed: () {
                          if (_userNameController.text.trim().isNotEmpty &&
                              _passwordController.text.trim().isNotEmpty &&
                              _passwordConfirmController.text
                                  .trim()
                                  .isNotEmpty &&
                              _emailController.text.trim().isNotEmpty) {
                            if (!RegExp(
                                    "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
                                .hasMatch(_emailController.text.trim())) {
                              showErrorScreen("Invalid email entered");
                            } else if (_passwordController.text
                                    .trim()
                                    .characters !=
                                _passwordConfirmController.text
                                    .trim()
                                    .characters) {
                              showErrorScreen(
                                  "Confirm password is incorrect");
                            } else if (_passwordController.text.trim().length <
                                6) {
                              showErrorScreen(
                                  "Password must be 6 characters or more");
                            } else {
                              register();
                            }
                          } else {
                            showErrorScreen(
                                "All fields must be completed to register");
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            "Sign Up",
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color:
                                    Theme.of(context).primaryIconTheme.color),
                          ),
                        )),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              )
            ],
          ),
        ),
      ),
    );
  }

  register() async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return LoadingAlertDialog(
            message: "Authenticating...",
            onBackButton: true,
          );
        });
    try {
      UserCredential userCredential =
          await _auth.createUserWithEmailAndPassword(
              email: _emailController.text.trim(),
              password: _passwordController.text.trim());
      await saveUserData(userCredential);
      await uploadProfile(profilePatch);
      await setColorIndex();
      FirebaseAuth.instance.authStateChanges().listen((User? user) {
        if (user != null) {
          Route newRoute = MaterialPageRoute(builder: (BuildContext context) {
            return HomePage();
          });
          Navigator.pushAndRemoveUntil(context, newRoute, (route) => false);
        }
      });
    } on FirebaseAuthException catch (e) {
      Navigator.pop(context);
      showErrorScreen(e.code);
    } catch (e) {
      print(e.toString());
    }
  }

  Future saveUserData(UserCredential userCredential) async {
    String userId = userCredential.user!.uid;

    Map<String, dynamic> userData = {
      'userName': _userNameController.text.trim(),
      'uId': userId,
      'time': DateTime.now(),
    };
    await FirebaseFirestore.instance
        .collection('users')
        .doc(userId)
        .set(userData);
  }

  Future uploadProfile(String filePath) async {
    if (filePath.isNotEmpty) {
      File file = File(filePath);
      try {
        await firebase_storage.FirebaseStorage.instance
            .ref('profile/${_emailController.text.trim()}-image-file.png')
            .putFile(file);
      } on firebase_storage.FirebaseException catch (e) {
        print(e.code);
      }
    }
  }

  Future _getImage(ImageSource imageSource) async {
    PickedFile? imageFile = await ImagePicker.platform
        .pickImage(source: imageSource, imageQuality: 100);

    if (imageFile != null) {
      setState(() {
        profilePatch = imageFile.path;
      });
    }
  }

  showErrorScreen(String message) {
    Route route = MaterialPageRoute(builder: (BuildContext context) {
      return ErrorAlertDialog(message: message);
    });
    Navigator.push(context, route);
  }

  Future<void> setColorIndex() async {
    await ColorManager().setColorIndex();
  }

  displayBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10))),
        builder: (context) {
          return Container(
            height: MediaQuery.of(context).size.height * 0.3,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text(
                    "Profile Image",
                    style: TextStyle(fontSize: 18),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        FloatingActionButton(
                          onPressed: () {
                            _getImage(ImageSource.camera);
                            Navigator.pop(context);
                          },
                          child: Icon(Icons.linked_camera),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text("Camera"),
                        )
                      ],
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        FloatingActionButton(
                          onPressed: () {
                            _getImage(ImageSource.gallery);
                            Navigator.pop(context);
                          },
                          child: Icon(Icons.image),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            "Gallery",
                          ),
                        )
                      ],
                    ),
                    profilePatch.isNotEmpty
                        ? Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              FloatingActionButton(
                                backgroundColor: Colors.red,
                                onPressed: () {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          content: Text(
                                              "Do you want delete profile image?"),
                                          actions: [
                                            TextButton(
                                                onPressed: () {
                                                  setState(() {
                                                    profilePatch = "";
                                                  });
                                                  Navigator.pop(context);
                                                  Navigator.pop(context);
                                                },
                                                child: Text("Yes")),
                                            TextButton(
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                  Navigator.pop(context);
                                                },
                                                child: Text("Cancel")),
                                          ],
                                        );
                                      });
                                },
                                child: Icon(
                                  Icons.delete,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("Delete Image"),
                              )
                            ],
                          )
                        : Text(""),
                  ],
                ),
              ],
            ),
          );
        });
  }
}
