# startup

 - Due to the lack of access to the Mac system, only the settings of the Android platform are applied to the project.

 - Startup is a Flutter app with Firebase Back-end.
 - # This app has the following features :

 - User registration.
 - Upload, delete and update profile picture in Firebase Storage.
 - Edit user information (example: user name).
 - Sign out of your account.
 - Login to account.
 - Delete account and all user information.

*************************************************
 - بدلیل عدم دسترسی به سیستم مک ،تنها تنظیمات پلتفرم اندروید بر روی پروژه اعمال شده است

 - استارت آپ یک اپ فلاتر با بک اند فایر بیس است.
 - # این برنامه شامل ویژگی های زیر است :

 - ثبت نام کاربر
 - آپلود و آپدیت تصویر پروفایل کاربر با دسترسی به گالری تصاویر و دوربین و همچنین امکان حذف تصویر پروفایل در Firebase Storage
 - ویرایش اطلاعات کاربر(برای مثال: user name ثبت شده هنگام ثبت نام)
 - خروج از حساب کاربری
 - ورود به حساب کاربری
 -  حذف حساب کاربری و تمام اطلاعات کاربر از سرور فایر بیس

